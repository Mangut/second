from flask import Flask, render_template, request
import numpy as np
import tensorflow as tf


app = Flask(__name__)

# model_loaded = tf.keras.models.load_model('model_1')

@app.route('/', methods=["get", "post"])
def predict():
    message = ""
    if request.method == "POST":
        IW = request.form.get("IW")
        IF = request.form.get("IF")
        VW = request.form.get("VW")
        FP = request.form.get("FP")

        vhod = [[float(IW), float(IF), float(VW), float(FP)]]

       # pred = model_loaded.predict(vhod)

       # print(pred)
        message = f"Вывожу введенные данные обратно {vhod}, чтобы проверить форму приложения."
    return render_template("index.html", message=message)




